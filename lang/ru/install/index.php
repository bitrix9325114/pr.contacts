<?php
$MESS["PR_CONTACTS_NAME"] = "Модуль для сохранения и вывода контактов";
$MESS["PR_CONTACTS_DESCRIPTION"] = "Добавляет поля контактов, которые можно вывести в публичной части";
$MESS["PR_CONTACTS_PARTNER_NAME"] = "";
$MESS["PR_CONTACTS_PARTNER_URI"] = "";
$MESS["PR_CONTACTS_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["PR_CONTACTS_INSTALL_TITLE"] = "Установка модуля";
$MESS["PR_CONTACTS_UNINSTALL_TITLE"] = "Деинсталляция модуля";