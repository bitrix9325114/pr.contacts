# Bitrix модуль контактов

## Table of Contents

- [Описание](#about)
- [Начало работы](#getting_started)
- [Использование](#usage)

## Описание <a name = "about"></a>

Модуль для сохранения и вывода контактов в публичной части шорткодами

## Начало работы <a name = "getting_started"></a>


### Установка

Описание установки


## Использование в публичной части <a name = "usage"></a>

Адрес: #ADDRESS# <br>
Номер телефона: #PHONE# <br>
Email: #EMAIL# <br>
Ватсап: #WHATSAPP# <br>
Телеграм: #TELEGRAM# <br>
