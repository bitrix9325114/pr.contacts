<?
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\EventManager;

Loc::loadMessages(__FILE__);

Class pr_contacts extends CModule {

    function __construct() {
        $arModuleVersion = array();

        include __DIR__. "/version.php";

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_ID = get_class($this);
        $this->MODULE_NAME = Loc::getMessage("PR_CONTACTS_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("PR_CONTACTS_DESCRIPTION");
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = Loc::getMessage("PR_CONTACTS_PARTNER_NAME");
        $this->PARTNER_URI = Loc::getMessage("PR_CONTACTS_PARTNER_URI");
    }

    public function InstallDB() {
        return false;
    }

    public function UnInstallDB()
    {
        Option::delete($this->MODULE_ID);

        return false;
    }

    public function InstallEvents()
    {

        EventManager::getInstance()->registerEventHandler(
            "main",
            "OnEndBufferContent",
            $this->MODULE_ID,
            "Pr\Contacts\Main",
            "changeContactsShortcodes"
        );

        return false;
    }

    public function UnInstallEvents()
    {

        EventManager::getInstance()->unRegisterEventHandler(
            "main",
            "OnEndBufferContent",
            $this->MODULE_ID,
            "Pr\Contacts\Main",
            "changeContactsShortcodes"
        );

        return false;
    }

    function DoInstall() {
        global $APPLICATION;

        if (CheckVersion(ModuleManager::getVersion("main"), "14.00.00")) {

            $this->InstallDB();
            $this->InstallEvents();

            ModuleManager::registerModule($this->MODULE_ID);

        } else {

            $APPLICATION->ThrowException(
                Loc::getMessage("PR_CONTACTS_INSTALL_ERROR_VERSION")
            );
        }

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("PR_CONTACTS_INSTALL_TITLE") . " \"" . Loc::getMessage("PR_CONTACTS_NAME") . "\"",
            __DIR__ . "/step.php"
        );
    }

    public function DoUninstall()
    {
        global $APPLICATION;

        $this->UnInstallDB();
        $this->UnInstallEvents();

        ModuleManager::unRegisterModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("PR_CONTACTS_UNINSTALL_TITLE") . " \"" . Loc::getMessage("PR_CONTACTS_NAME") . "\"",
            __DIR__ . "/unstep.php"
        );
    }
}
