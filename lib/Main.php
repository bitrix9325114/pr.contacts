<?php

namespace Pr\Contacts;

use Bitrix\Main\Config\Option;

class Main
{

    public function changeContactsShortcodes(&$content)
    {

        if (!defined("ADMIN_SECTION") && ADMIN_SECTION !== true) {

            $module_id = pathinfo(dirname(__DIR__))["basename"];

            $search = array(
                '#ADDRESS#',
                '#PHONE#',
                '#EMAIL#',
                '#TELEGRAM#',
                '#WHATSAPP#'
            );
            $replace = array(
                Option::get($module_id, "address", ""),
                Option::get($module_id, "phone", ""),
                Option::get($module_id, "email", ""),
                Option::get($module_id, "telegram", ""),
                Option::get($module_id, "whatsapp", "")
            );
            $content = str_replace($search, $replace, $content);
        }

        return false;
    }
}